# Pizza
Denne pakken viser hvordan en kan teste ulike størrelser av en enhet (unit). Enhetstesting er en superviktig komponent av testing. Dere vil se hvordan man tester at ulike ITopping virker om de skal, at Pizza virker som den skal (gjennom implementasjonen BasicPizza) og til slutt hvordan PizzaStore virker.

Dere vil i PizzaStore se at jeg ender opp med å finne et problem - getDescription som man får dekorert (ikke pensum) i BasicPizza sin addTopping returnerer fyll i motsatt rekkefølge. Jeg lar ting være slik de er, men synes det er et godt eksemepel på hvordan man kan finne feil i kode selv om det har blitt kjørt. Husk, testene bruker akkurat den samme koden som vi kjørte i brukergrensesnittet i uke 12.

Det er også en test som jeg med vilje lar feile, som også viser et problem som ikke lett lar seg løse uten litt omskriving. En ITopping burde ideelt være lik en annen dersom getName() og getCost() er like. Problemet er at man ikke kan lage en default equals-metode (default er ikke pensum) i et interface. Én løsning kan være å kreve at alle som implementerer ITopping faktisk også implementerer equals. På den måten vil en faktisk også få et interface som holder seg innenfor pensum.

# timeintervals123
Denne inkluderer jeg så vi kan se hvordan man kan teste for unntak vi forventer å få. Koden i Pizza var laget for å ignorere slike ting, så da blir det vanskelig å teste...