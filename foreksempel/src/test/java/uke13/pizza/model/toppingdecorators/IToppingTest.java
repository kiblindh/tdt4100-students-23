package uke13.pizza.model.toppingdecorators;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class IToppingTest {

    @Test
    public void testIToppings() {
        ITopping cheese = new Cheese(2.5);
        assertEquals("Cheese", cheese.getName());
        assertEquals(2.5, cheese.getCost());

        ITopping ham = new Ham(3.0);
        assertEquals("Ham", ham.getName());
        assertEquals(3.0, ham.getCost());

        ITopping mushroom = new Mushroom(1.5);
        assertEquals("Mushroom", mushroom.getName());
        assertEquals(1.5, mushroom.getCost());

        ITopping olives = new Olives(1.5);
        assertEquals("Olives", olives.getName());
        assertEquals(1.5, olives.getCost());

        ITopping onions = new Onions(1.0);
        assertEquals("Onions", onions.getName());
        assertEquals(1.0, onions.getCost());

        ITopping pepperoni = new Pepperoni(2.0);
        assertEquals("Pepperoni", pepperoni.getName());
        assertEquals(2.0, pepperoni.getCost());

        ITopping pineapple = new Pineapple(2.0);
        assertEquals("Pineapple", pineapple.getName());
        assertEquals(2.0, pineapple.getCost());
    }

    /*
     * Skulle gjerne ha testet at to skinke med lik verdi faktisk er like! Men da skulle jeg ha 
     * kunnet lage en generell metode i ITopping som overskriver equals. Det får jeg derimot ikke lov til
     * Se https://stackoverflow.com/questions/24016962/java8-why-is-it-forbidden-to-define-a-default-method-for-a-method-from-java-lan
     * Dette ligger utenfor pensum!
     * 
     * Jeg skisserer en mulig løsning på dette, via en abstrakt klasse mellom ITopping og Cheese. 
     * Den abstrakte klassen kan implementere equals. Ikke gjort, ikke fokus for tester eller faget.
     */
    @Test
    public void testToppingEqualsOtherToppingWithSameNameAndCost() {
        ITopping pineapple = new Pineapple(2.0);
        assertEquals(pineapple, new Pineapple(2.0));
    }
}
