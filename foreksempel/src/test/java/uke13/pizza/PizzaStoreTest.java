package uke13.pizza;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import uke13.pizza.model.PizzaStore;
import uke13.pizza.model.toppingdecorators.Cheese;
import uke13.pizza.model.toppingdecorators.Ham;
import uke13.pizza.model.toppingdecorators.ITopping;
import uke13.pizza.model.toppingdecorators.Mushroom;
import uke13.pizza.model.toppingdecorators.Olives;
import uke13.pizza.model.toppingdecorators.Onions;
import uke13.pizza.model.toppingdecorators.Pepperoni;
import uke13.pizza.model.toppingdecorators.Pineapple;
import uke13.pizza.util.PizzaCosts;

public class PizzaStoreTest {

    static PizzaStore pizzaStore;

    @BeforeEach
    public void polulatePizzaStore() {
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(PizzaCosts.CHEESE));
        availableToppings.add(new Ham(PizzaCosts.HAM));
        availableToppings.add(new Mushroom(PizzaCosts.MUSHROOM));
        availableToppings.add(new Olives(PizzaCosts.OLIVES));
        availableToppings.add(new Onions(PizzaCosts.ONIONS));
        availableToppings.add(new Pepperoni(PizzaCosts.PEPPERONI));
        availableToppings.add(new Pineapple(PizzaCosts.PINEAPPLE));
    
        // Create a PizzaStore instance
        pizzaStore = new PizzaStore(availableToppings);
    }

    @Test
    public void testOrderPizzaFromStringLowercase() {
        pizzaStore.addToOrder("John", "cheese");
        assertEquals("Basic Pizza, Cheese",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
    }

    @Test
    public void testOrderPizzaFromStringUppercase() {
        pizzaStore.addToOrder("John", "Cheese");
        assertEquals("Basic Pizza, Cheese",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
    }

    @Test
    public void testOrderPizzaSkipNonexistingTopping() {
        pizzaStore.addToOrder("John", "Cheese, Løk");
        assertEquals("Basic Pizza, Cheese",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
    }

    @Test
    public void testOrderPizzaEmptyToppingsGivesBasicizza() {
        pizzaStore.addToOrder("John", "");
        assertEquals("Basic Pizza",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
    }

    @Test
    public void testOrderPizzaFromStringTwoIngredients() {
        pizzaStore.addToOrder("John", "Cheese, Onions");
        // assertEquals("Basic Pizza, Cheese, Onions",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
        
        // Hva er galt her? Vi kan se at rekkefølgen på toppings kommer i motsatt rekkefølge av slik det ble
        // lagt på. Det viser seg at det er en bivirkning av BasicPizza.addTopping. som bygger opp
        // getDescription() som etter decorator-teknikken. Det er utenfor pensum å skulle begynne å 
        // se på dette, men jeg tar det med for å vise at man gjennom å skrive tester kan begynne å
        // se hvorvidt koden faktisk representerer det en ønsker. Så må man avgjøre hva man gjør videre!

        // Det som er merkelig er jo at dette problemet ikke kunne ses fra PizzaTest...
        // Det er jo fordi jeg der tester hva hver enkelt topping er, de ligger jo i den riktige rekkefølgen
        // Det er getDescription som legger toppings motsatt.
        System.out.println(pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
        assertEquals("Basic Pizza, Onions, Cheese",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());

    }

    // Nå som jeg har funnet problemet med rekkefølgen på toppings, og funnet ut at jeg er komfortabel med at
    // rekkefølgen er grei, så skriver jeg bare testkoden slik at den passer. Men jeg burde kanskje heller ha fikset det. ;)
    @Test
    public void testOrderPizzaFromStringTwoPizzas() {
        pizzaStore.addToOrder("John", "Cheese, Onions");
        pizzaStore.getCustomerOrders().get("John").forEach(p -> System.out.println(p.getToppings()));
        pizzaStore.addToOrder("John", "Ham, Olives, Cheese");
        pizzaStore.getCustomerOrders().get("John").forEach(p -> System.out.println(p.getToppings()));
        // assertEquals("Basic Pizza, Cheese, Onions",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
        assertEquals("Basic Pizza, Onions, Cheese",pizzaStore.getCustomerOrders().get("John").get(0).getDescription());
        // assertEquals("Basic Pizza, Ham, Olives, Cheese",pizzaStore.getCustomerOrders().get("John").get(1).getDescription());
        assertEquals("Basic Pizza, Cheese, Olives, Ham",pizzaStore.getCustomerOrders().get("John").get(1).getDescription());
    }

    @Test
    public void testTwoCustomers() {
        pizzaStore.addToOrder("Bob", "cheese");
        pizzaStore.addToOrder("Violet", "ham");
        assertEquals("Basic Pizza, Cheese",pizzaStore.getCustomerOrders().get("Bob").get(0).getDescription());
        assertEquals("Basic Pizza, Ham",pizzaStore.getCustomerOrders().get("Violet").get(0).getDescription());
    }
}





