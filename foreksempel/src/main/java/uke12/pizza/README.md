# Mål tirsdag
- Gjøre ferdig pizzasjappa.
- Ikke la brukergrensesnittet bli topp. Kopiere inn det som er ferdiglaget etter at en ser at deet nye virker.
- Fortelle om den fine feilen da du la inn fx:id på en VBox og ikke en Label.
- Se om en kan introdusere noe nytt, behov hos kunden som utløser endring i modellen.
- Myse på andre eksempler - 
- Hvis masse tid, kanskje noe om testing.

# Pizza - en pizzabutikk med tilhørende brukergrensesnitt
Dette eksempelet er laget for å vise hvordan vi tenker at man kan gjennomføre MVC - model-view-controller-prinsippet. Vi skal ha en fullt kjørende modell. Dette er koden til selve systemet vi ønsker å endre tilstandene ved å interagere med det. Det kjører via mainmetoden (eksempel lagt i [PizzaStore](./model/PizzaStore.java)). I tillegg finnes det to eksempler på hvordan det kjører via 'grensesnitt'. Det er et svært enkel tekstgrensesnitt i [PizzaTextController](./controller/PizzaTextController.java). Husk å sette   *"console": "integratedTerminal"* for denne filen i launch.json, hvis ikke får en ikke skrevet inn noe. Til slutt har vi det grafiske grensesnittet, representert ved controlleren [MyPizzaController](./controller/MyPizzaController.java) og medhørende filer.

Jeg bruker en del ting som ligger på utsiden av pensum, en del av disse tingene er det mange av dere som uansett ønsker å kunne bruke i prosjektet, eller dere bare har interesse av å studere. Eksempler på dette er (ikke fullstendig):
- Interface har [default-metoder](./model/Pizza.java). Siden vi vet hvilke metoder som blir implementert kan vi bruke dem!
- Bruk av Logger for å [lagre filer](./model/listeners/CheeseLover.java). Denne er implementert for å vise at en kan bestemme hvordan ting skal skrives ut (til System.err), og Logger er fryktelig mye brukt. Når dere kjører main i PizzaStore vil dere se at beskjeder fra CheeseLover vil skrives ut i et annet format enn annen tekst. 
- [Dekorasjonsteknikken](https://www.baeldung.com/java-decorator-pattern). Denne er artig, og brukes mye 'der ute'. Prinsippet ved den involverer ikke noe nytt ut over pensum, men selve implementasjonen her gjør nok det. 
- Ta over System.out. I [PizzaController](./controller/MyPizzaController.java) sin initialize-metode tar jeg over hvor System.out og System.err, slik av eventuelle ting som sendes til disse to går til et tekstfelt. Den bruker hjelpeklassen [DebugTextAreaOutputStream](./util/DebugTextAreaOutputStream.java).

NB: Det aller meste av modellen som ender i PizzaStore ble skrevet av ChatGPT! Jeg gjorde dette for å se litt mer hvordan man kunne bruke en slik tjeneste til å vise én dekorasjonsteknikken. ChatGPT og liknende verktøy kan brukes til masse godt - men også som en krykke som gjør at en ikke egentlig lærer noe av det som gjøres. Jeg vil oppmuntre til å ha en aktiv holdning til dette. Bruk det gjerne til å beskrive hva kode gjør for deg - eller for å forklare hvorfor feil oppstår. I løpet av degge prosjektet har jeg også sett hvor ofte jeg har måttet lede verktøyet inn på rett spor. Det er først når en *har nok* bakgrunnskunnskap at en kan bruke det på en nytt måte. Men kan ikke si noe annet enn at det er utrolig artig, har ledd mange ganger i løpet av denne sesjonen. 

Dette eksempelet er ikke ment å krysse av alle kravene til prosjektet deres. Jeg har blant annet ikke laget noe system som oppretter tilstand fra fil. Det er hva jeg fikk laget da jeg satt meg ned et par dager. I tillegg er det jo en del ting som er utenfor pensum, men jeg vet at en del av dere faktisk ønsker å ledes inn i videre ting. Er det noe som kan fungere som inspirasjon, er det topp. Husk også at det er massevis av måter å gjøre ting, de løsningene jeg (og ChatGPT!) har valgt er kun eksempler.

## Jeg får ikke kjørt appen!
Jeg *tror* at det eneste som skal hindre dere fra å kjøre dette er at jeg har brukt Logger. Denne tror jeg ikke dere finner i standardinstallasjonen dere har. Men det finnes en løsning på det. Gå til filen [module-info.txt](../../module-info.java) og legg til følgende linje: **requires java.logging;**

Oppdatert: Jeg har nå lagt til denne linjen i module-info.txt og dyttet den på git, så du skal ikke få noen problemer. Velger allikevel å la dette stå. En typisk som man kan ende opp i er at man legger til noe slikt nytt, og så vil ikke koden kjøre. Ved kjøring er det liksom som Logger ikke finnes - selv om man kan bruke klassen fint under utvikling. En typisk feil er da at ting ikke er oppdatert i modules-info.txt.  

## Struktur
Koden er delt opp i ulike (del)pakker, der  

## Modellen - PizzaStore
I dette eksempelet bruker jeg en måte å strukturere kode som i og for seg ikke er pensum - dekorasjon. 
Ting som jeg bare kort har nevnt, som at interface kan ha default-metoderJeg vil ikke gå i detalj om hvordan dette gjøres, men i kortform kan en si at det likner litt på lambda stream sin måte å kjede kall til metode og konstruktører. Man bygger opp en pizza gjennom stadige kall til addTopping. Det er ikke forventet at dere skal kunne kjenne igjen hva som skjer sånn uten videre. Det viktigste er å se på [PizzaStore](./model/PizzaStore.java) og hvilke muligheter den gir.

*Koden i modellen er ikke ment å representere hva dere må kunne i faget, hovedsaken denne uken er å se hvordan man kan knytte en modell opp mot et brukergrensesnitt eller to.*

### PizzaStore
Denne klassen inneholder et sett med ITopping som man kan bruke til å bygge opp en pizza. Pizzaer kan så bestilles av ulike kunder - man kan bestille flere pizzaer. Systemet holder rede på kostnad for hver enkelt kunde. I eksempelet har jeg ikke laget noe system for betaling - jeg forutsetter bare at betaling skjer ved utlevering. Jeg har beskrevet raskt i koden hva de ulike metodene gjør, men det er ikke mye utrolig spesielt.

### Observatør - observert
Det er to lyttere til hendelser i PizzaStore. De implementerer interfacet IPizzaListener. Hele sjappa sendes med, men det brukes ikke til noe inntil videre. Håper jeg har kapslet inn godt nok slik at ingen kan rote!
- [CheeseLover](./model/listeners/CheeseLover.java): Denne reagerer på alle pizzaer som bestilles, og lagrer til en fil dersom pizzaen inneholder ingrediensen Cheese.
- [ProfitListener](./model/listeners/ProfitListener.java): Denne holder koll på inntekten til sjappa, på en ganske enkel måte. Kostnaden ved å lage en pizza er 30% av prisen - flatt. Denne trekkes fra idét bestillingen skjer. Når kunden så plukker opp bestillingen legges hele prisen til. Alt skrives ut via vanlig utskrift (som dere vil se kidnappes av et tekstfelt når det kjører som app) men jeg har også kommentert ut hvordan en kunne brukt Logger.  

## Grensesnittene - grafikk og tekst
### Tekstgrensesnitt
[PizzaTextController](./controller/PizzaTextController.java) er en interaktiv tekstversjon av applikasjonen. Dog en smule forenklet. Det er derimot en skjult fordel i klassen - man kan ikke bestille dobbel ost via JavaFX-versjonen! Husk å endre på launch.json så koden kjøres i terminalen og ikke i debuggeren.
### JavaFX - MyPizzaApp
Grensesnittet består av to hovedklasser - [controlleren](./controller/MyPizzaController.java) og [appen](./controller/MyPizzaApp.java). I tillegg benyttes en [hjelpeklasse](./util/DebugTextAreaOutputStream.java) for å sende alle beskjeder til System.out/err til et tekstfelt i stedet.

Grensesnittet selv er definert i [pizza.fxml](./controller/pizza.fxml), og benytter en kombinasjon av VBox og HBox for å lage noe som ser sånn noenlunde ut. Ikke grensesprengende utseende, men får jobben gjort. Det er fryktelig mange måter å bygge opp grensesnitt, jeg anbefaler at man bare forsøker seg med litt ulike. Det er derimot et hovedprinsipp:
- Knytt grensesnittet mot en kontroller i menyen nede til venstre i SceneBuilder.
- Husk hierarkimenyen til venstre! Dragondrop funker topp her. ;)
- Pane er lerrett som man kan legge elementer inn i. Også nye panes.
- Ulike typer Pane har ulike måter å distribuere elementene.
- De to tingene jeg har gjort mest i Panes er i menyene til høyre i SceneBuilder:
    - Properties - node - alignment: Skal innholdet legges mot venstre, i midten etc.
    - Layout - '(noe med) grow': hvordan skal elementet endres når hele appen endrer størrelse.
    - Layout - margins: unngå at elementene går helt ut i kantene.
    - Layout - spacing: mellomrom mellom elementene
- De tingene jeg har gjort mest med elementer som knapper og tekstfelt:
    - Properties - font: Gjett da!
    - Code - fx:id: Knytte elementet til noe som har @FXML i kontrolleren. Hvis du har satt opp kontrolleren og knyttet den opp vil alternativene vises.
    - Code - onAction: Gjelder for Button (sikkert flere). Når knappen trykkes inn skal følgende metode i kontrolleren kalles.
    - Code - masse rart. Her kan en boltre seg i hva som kan skje når brukeren gjør ulike ting. 

### Hva skjer når appen kjøres (kort forklart)
Dette er ikke den eneste måten man lager brukergrensesnitt i JavaFX, men den vi lærer bort. Teksten under reflekterer implementasjonsvalget gjort her.

- Når man kjører MyPizzaApp så vil etterhvert metoden *start* kalles (uten at du må gjøre noe). I denne metoden peker vi til den korrekte fxml-filen, og viser noe som heter Stage. 
- Tenk at Stage er den heeelt ytterste kontaineren, altså selve programvinduet du skal starte opp. Denne kan settes til størrelse, en tittel, slike ting. 
- Stage kan sette en Scene. Det er selve grensesnittet som vi ønsker å vise inni Stage-vinduet. Her kobler vi inn fxml-filen!
- Når Stageobjektet sin show() kalles vil selve vinduet vises.

Men vi er ikke ferdige der!

- fxml-filen har et sett med objekter som skal tegnes i vinduet. Filen har også en kobling til en kontroller, og det er i denne kontrolleren vi kjører kode (det er jo kode i MyPizzaApp også))
- I kontrolleren er det definert et sett med felt og metoder som har @FXML foran seg. Dette indikerer at de på en eller annen måte skal knyttes til elementer i brukergrensesnittet. De 'lages'.
- Så kalles kontrollerens *initialize*-metode automatisk. Her gjøres, naturlig nok, det som skal til for å få opp systemet i den tilstanden en ønsker ved oppstart. Ting som skjer:
    - Yoinke System.out og System.err ved å gi System en annen printStream å bruke, så vi kan se utskrifter i et tekstfelt.
    - Mekke en PizzaStore, initiert med et sett med ITopping.
    - Lage et par IPizzaListeners og la dem abonnere på PizzaStoreoppdateringer.
    - Tilpasse en ListView så man kan velge multiple toppinger på pizzaen.
    - Legge alle toppinger som finnes i PizzaStore inn i nevnte ListView.
    - I tillegg kalles *updateGUI*. Denne metoden kalles hver gang det har skjedd noe som kan/bør resultere i en endring av brukergrensesnittet.  
    