package uke12.pizza.model.listeners;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import uke12.pizza.model.Pizza;
import uke12.pizza.model.PizzaStore;

public class ProfitListener implements IPizzaListener {

    // private static final Logger logger = Logger.getLogger(ProfitListener.class.getName());
    private double totalProfit;
    private ArrayList<Pizza> soldPizzas = new ArrayList<>();

    @Override
    public void pizzaAdded(PizzaStore pizzaStore, Pizza pizza) {
        double loss = pizza.getCost() * 0.3;
        System.out.println("Pizza ordered - Loss: "+loss);
        // logger.log(Level.INFO, "Pizza ordered - Loss: {0}", loss);
        updateProfit(-loss);
        soldPizzas.add(pizza);
    }

    @Override
    public void pizzaRemoved(PizzaStore pizzaStore, Pizza pizza) {
        double profit = pizza.getCost();
        System.out.println("Pizza sold - Profit: "+profit);
        // logger.log(Level.INFO, "Pizza sold - Profit: {0}", profit);
        updateProfit(profit);
        soldPizzas.remove(pizza);
    }

    private void updateProfit(double amount) {
        totalProfit += amount;
        System.out.println("Total profit: "+ totalProfit);
        // logger.log(Level.INFO, "Total profit: {0}", totalProfit);
    }

    public double getTotalProfit() {
        return totalProfit;
    }
}
