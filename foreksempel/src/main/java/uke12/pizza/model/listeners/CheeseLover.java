package uke12.pizza.model.listeners;

import java.io.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import uke12.pizza.model.Pizza;
import uke12.pizza.model.PizzaStore;
import uke12.pizza.model.toppingdecorators.Cheese;

/*
 * En observatør som leter etter når det legges inn en ny pizza som inneholder 
 * ingrediensen Cheese.
 * Den bruker også Logger for å vise hvordan man kan bruke dette til å skrive
 * til fil. Denne måten uavhengig av om koden kjøres som en app eller kommandolinje.
 */
public class CheeseLover implements IPizzaListener {

    private static final Logger LOGGER = Logger.getLogger(CheeseLover.class.getName());
    private static final String FILE_NAME = "cheese_lover.txt";
    private static final Object FILE_LOCK = new Object();
    private static BufferedWriter fileWriter;

    static {
        try {
            FileHandler fileHandler = new FileHandler(FILE_NAME, true);
            System.out.println(fileHandler);
            fileHandler.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(fileHandler);
            LOGGER.setLevel(Level.INFO);
            fileWriter = new BufferedWriter(new FileWriter(FILE_NAME, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void pizzaAdded(PizzaStore pizzaStore, Pizza pizza) {
        if (pizza.hasTopping(new Cheese(0))) {
            LOGGER.info("CheeseLover: found cheese pizza (yum): " + pizza.getDescription());
            writeToFile(pizza.getDescription() + " (" + pizza.getCost() + ")\n");
        }
    }

    @Override
    public void pizzaRemoved(PizzaStore pizzaStore, Pizza pizza) {
        // do nothing
    }

    private static void writeToFile(String message) {
        synchronized (FILE_LOCK) {
            try {
                fileWriter.write(message);
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
