package uke12.pizza.model.listeners;

import uke12.pizza.model.Pizza;
import uke12.pizza.model.PizzaStore;

public interface IPizzaListener {
    void pizzaAdded(PizzaStore store, Pizza pizza);
    void pizzaRemoved(PizzaStore store, Pizza pizza);
}
