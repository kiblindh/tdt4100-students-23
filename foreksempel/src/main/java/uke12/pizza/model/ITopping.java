package uke12.pizza.model;

public interface ITopping {
    public String getName();
    public double getCost();
}

