package uke12.pizza.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import uke12.pizza.model.listeners.CheeseLover;
import uke12.pizza.model.listeners.IPizzaListener;
import uke12.pizza.model.listeners.ProfitListener;
import uke12.pizza.model.toppingdecorators.Cheese;
import uke12.pizza.model.toppingdecorators.Ham;
import uke12.pizza.model.toppingdecorators.ITopping;
import uke12.pizza.model.toppingdecorators.Mushroom;
import uke12.pizza.model.toppingdecorators.Olives;
import uke12.pizza.model.toppingdecorators.Onions;
import uke12.pizza.model.toppingdecorators.Pepperoni;
import uke12.pizza.model.toppingdecorators.Pineapple;

public class PizzaStore {
    private List<ITopping> toppings; // Hva kan vi ha på pizzaen
    private Map<String, List<Pizza>> customerOrders; // Hvem bestiller
    private List<IPizzaListener> listeners; // hvem lytter

    /* I konstruktøren valgte jeg å sende inn en liste med toppings. Det er ikke noen
     * spesiell grunn til at jeg valgte dette sammenliknet med å ha en tom konstruktør
     * og bygge den opp med addStoreTopping elns.  
     */
    public PizzaStore(List<ITopping> availableToppings) {
        this.toppings = availableToppings;
        this.customerOrders = new HashMap<>();
        this.listeners = new ArrayList<>();
    }

    // Hvilke toppings vi har må vises frem i brukergrensesnittene, så denne må vi ha!
    // Returnerer en kopi, så vi ikke kan rote med innholdet.
    public List<ITopping> getToppingsList() {
        return toppings.stream().collect(Collectors.toList());
    }

    /*
     * To metoder for å lage en pizza. Den ene splitter opp en streng med ingredienter, den
     * andre tar inn en liste med toppings. Felles for dem er at de gradvis bygger opp pizzaen
     * gjennom å legge topping på en BasicPizza. Til slutt returneres denne.
     * 
     */
    public Pizza createPizza(String toppingsList) {
        String[] toppingsToAdd = toppingsList.split(",");
        Pizza pizza = new BasicPizza();

        for (String toppingName : toppingsToAdd) {
            for (ITopping topping : toppings) {
                if (toppingName.trim().equalsIgnoreCase(topping.getName())) {
                    pizza = pizza.addTopping(topping);
                    break;
                }
            }
        }

        return pizza;
    }

    // Kommentar se over
    public Pizza createPizza(List<ITopping> toppings) {
        Pizza pizza = new BasicPizza();

        for (ITopping topping : toppings) {
            pizza = pizza.addTopping(topping);
        }

        return pizza;
    }


    // To metoder for å lage en pizza og knytt den til en kunde. Her med streng inn 
    public void addToOrder(String customerName, String toppingsList) {
        Pizza pizza = createPizza(toppingsList);
        addToOrder(customerName, pizza);
    }

    // .. her med en ferdig pizza som parameter
    public void addToOrder(String customerName, Pizza pizza) {
        if (!customerOrders.containsKey(customerName)) {
            customerOrders.put(customerName, new ArrayList<>());
        }
        customerOrders.get(customerName).add(pizza);
        notifyPizzaAdded(pizza);
    }

    public Map<String, List<Pizza>> getCustomerOrders() {
        return customerOrders;
    }

    // Hente ut pizzaene til en kunde. At ChatGPT foreslo å returnere totalpris bruker
    // jeg ikke, men valgte å beholde. 
    public double collectPizzas(String customerName) {
        double totalCost = 0.0;
        if (customerOrders.containsKey(customerName)) {
            List<Pizza> pizzas = customerOrders.get(customerName);
            for (Pizza pizza : pizzas) {
                totalCost += pizza.getCost();
                notifyPizzaRemoved(pizza); // Gi beskjed til lyttere
            }
            customerOrders.remove(customerName);
        }
        return totalCost;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        double totalValue = 0.0;

        for (String customerName : customerOrders.keySet()) {
            sb.append(customerName).append(" ordered:\n");

            double totalCost = 0.0;
            List<Pizza> pizzas = customerOrders.get(customerName);
            for (Pizza pizza : pizzas) {
                sb.append("- ").append(pizza.getDescription()).append(": ").append(pizza.getCost()).append(" kr\n");
                totalCost += pizza.getCost();
            }

            sb.append("Total cost for ").append(customerName).append(": ").append(totalCost).append(" kr\n\n");
            totalValue += totalCost;
        }

        sb.append("Total value of all orders: ").append(totalValue).append(" kr\n");
        return sb.toString();
    }

    // Så fire metoder som brukes til lyttere. Rett ut av pensum!

    public void notifyPizzaAdded(Pizza pizza) {
        for (IPizzaListener listener :  listeners) {
            listener.pizzaAdded(this, pizza);
        }
    }
    
    public void notifyPizzaRemoved(Pizza pizza) {
        for (IPizzaListener listener : listeners) {
            listener.pizzaRemoved(this, pizza);
        }
    }
    
    public void addListener(IPizzaListener listener) {
        listeners.add(listener);
    }
    
    public void removeListener(IPizzaListener listener) {
        listeners.remove(listener);
    }
    
    public int getNumberOfCustomers() {
        return customerOrders.size();
    }

    public static void main(String[] args) throws IOException {
        // Create a list of available toppings
        List<ITopping> availableToppings = new ArrayList<>();
        availableToppings.add(new Cheese(2.5));
        availableToppings.add(new Ham(3.0));
        availableToppings.add(new Mushroom(1.5));
        availableToppings.add(new Olives(1.5));
        availableToppings.add(new Onions(1.0));
        availableToppings.add(new Pepperoni(2.0));
        availableToppings.add(new Pineapple(2.0));
    
        // Create a PizzaStore instance
        PizzaStore pizzaStore = new PizzaStore(availableToppings);
    
        CheeseLover cheeseLover = new CheeseLover();
        ProfitListener profitListener = new ProfitListener();
        pizzaStore.addListener(cheeseLover);
        pizzaStore.addListener(profitListener);
        System.out.println("Added listeners");

        pizzaStore.addToOrder("Børge", "cheese, ham, mushroom");
        pizzaStore.collectPizzas("Børge");
        // Create some pizzas and add them to the order for "Alice"
        Pizza pizza1 = pizzaStore.createPizza("cheese, ham, mushroom");
        Pizza pizza2 = pizzaStore.createPizza("cheese, pineapple");
        pizzaStore.addToOrder("Alice", pizza1);
        pizzaStore.addToOrder("Alice", pizza2);
    
        // Create another pizza and add it to the order for "Bob"
        Pizza pizza3 = pizzaStore.createPizza(List.of(new Cheese(2.5), new Pepperoni(2.0), new Olives(1.5)));
        pizzaStore.addToOrder("Bob", pizza3);
        pizzaStore.addToOrder("Børge", "cheese, ham, mushroom");
        pizzaStore.addToOrder("Børge", "cheese, pineapple, cheese");
        pizzaStore.addToOrder("Børge", "ham, olives");
    
        // System.out.println(pizzaStore);
    
        // Collect the pizzas for "Alice" and print the total cost
        double aliceTotalCost = pizzaStore.collectPizzas("Alice");
        System.out.println("Alice paid " + aliceTotalCost + " for her pizzas.");

        System.out.println(pizzaStore);
        // Collect the pizzas for "Børge" and print the total cost
        double bTotalCost = pizzaStore.collectPizzas("Børge");
        System.out.println("@ paid " + bTotalCost + " for its pizzas.");
    }
    
}
