package uke12.pizza.model.toppingdecorators;

public class Mushroom implements ITopping {
    private double cost;

    public Mushroom(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Mushroom";
    }

    public double getCost() {
        return cost;
    }
}
