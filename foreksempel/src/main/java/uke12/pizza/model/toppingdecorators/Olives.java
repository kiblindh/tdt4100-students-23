package uke12.pizza.model.toppingdecorators;

public class Olives implements ITopping {
    private double cost;

    public Olives(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Olives";
    }

    public double getCost() {
        return cost;
    }
}
