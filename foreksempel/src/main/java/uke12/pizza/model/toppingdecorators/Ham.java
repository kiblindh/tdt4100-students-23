package uke12.pizza.model.toppingdecorators;

public class Ham implements ITopping {
    private double cost;

    public Ham() {
        this.cost = 1.25;
    }

    public Ham(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Ham";
    }

    public double getCost() {
        return cost;
    }
}
