package uke12.pizza.model.toppingdecorators;

public interface ITopping {
    public String getName();
    public double getCost();
}

