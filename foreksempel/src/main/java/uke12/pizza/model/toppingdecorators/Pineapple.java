package uke12.pizza.model.toppingdecorators;

public class Pineapple implements ITopping {
    private double cost;

    public Pineapple(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Pineapple";
    }

    public double getCost() {
        return cost;
    }
}
