package uke12.pizza.model;

import uke12.pizza.model.toppingdecorators.ITopping;

public abstract class ToppingDecorator implements Pizza {
    protected Pizza pizza;

    public ToppingDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    public String getDescription() {
        return pizza.getDescription();
    }

    public double getCost() {
        return pizza.getCost();
    }

    public Pizza addTopping(ITopping topping) {
        pizza = pizza.addTopping(topping);
        return this;
    }
}
