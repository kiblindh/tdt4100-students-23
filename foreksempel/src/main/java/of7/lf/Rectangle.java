package of7.lf;

import javafx.scene.paint.Color;

public class Rectangle extends Shape {

    public Rectangle(Color color, double length, double height) {
        super(color);
        this.height = height;
        this.length = length;
    }

    private double length;

    private double height;

    @Override
    public String toString() {
        return "Rektangel, " + super.toString();
    }

    @Override
    public double getArea() {
        return length * height;
    }

}
