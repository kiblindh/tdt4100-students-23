package of7.lf;

import javafx.scene.paint.Color;

public class Vehicle {

    private String bilmerke;

    private String model;

    private int registreringsår;

    private Color farge;

    public Vehicle(String bilmerke, String model, int registreringsår, Color farge) {
        this.bilmerke = bilmerke;
        this.model = model;
        this.registreringsår = registreringsår;
        this.farge = farge;
    }

    @Override
    public String toString() {
        return "Vehicle [bilmerke=" + bilmerke + ", model=" + model + ", registreringsår=" + registreringsår
                + ", farge=" + farge + "]";
    }

}
