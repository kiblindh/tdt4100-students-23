package of7.lf;

import javafx.scene.paint.Color;

public class Bil extends Vehicle {

    private int antallDører;

    public Bil(String bilmerke, String model, int registreringsår, Color farge, int antallDører) {
        super(bilmerke, model, registreringsår, farge);
        this.antallDører = antallDører;
    }

    @Override
    public String toString() {
        return super.toString() + "Bil [antallDører=" + antallDører + "]";
    }

    public static void main(String[] args) {
        Vehicle v1 = new Vehicle("Red Bull", "??", 2023, Color.BLUE);

        Vehicle v2 = new Truck("Tesla", "Semi", 2018, Color.WHITE, 20000);

        Bil c1 = new Bil("Tesla", "Y", 2022, Color.RED, 5);

        System.out.println(v1);
        System.out.println(v2);
        System.out.println(c1);

    }

}
