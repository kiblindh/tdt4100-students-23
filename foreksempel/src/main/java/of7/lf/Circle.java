package of7.lf;

import javafx.scene.paint.Color;

public class Circle extends Shape {

    private double radius;

    public Circle(Color color, double radius) {
        super(color);
        this.radius = radius;
        
    }

    @Override
    public double getArea() {
        return Math.pow(radius,2)* Math.PI;
    }

    @Override
    public String toString() {
        return "Sirkel, " + super.toString();
    }

    public static void main(String[] args) {
        Shape s1 = new Circle(Color.RED, 10);
        Shape s2 = new Rectangle(null, 20, 10);
        System.out.println(s1);
        System.out.println(s2);
    }
    
}
