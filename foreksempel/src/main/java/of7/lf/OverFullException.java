package of7.lf;

public class OverFullException extends RuntimeException {

    public OverFullException() {
        super("Det er ikke plass :(");
    }
}
