package uke13.pizza.model.listeners;

import uke13.pizza.model.Pizza;
import uke13.pizza.model.PizzaStore;

public interface IPizzaListener {
    void pizzaAdded(PizzaStore store, Pizza pizza);
    void pizzaRemoved(PizzaStore store, Pizza pizza);
}
