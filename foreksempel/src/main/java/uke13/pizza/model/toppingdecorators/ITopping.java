package uke13.pizza.model.toppingdecorators;

public interface ITopping {

    public String getName();
    public double getCost();


/* Følgende equals er ikke lov, selv om jeg gjerne skulle hatt det, så praktisk det hadde vært!
 * https://stackoverflow.com/questions/24016962/java8-why-is-it-forbidden-to-define-a-default-method-for-a-method-from-java-lan
 * 
 * I den abstrakte klassen UbruktAbstractTopping skisserer jeg en løsning på dette, men jeg 
 * implementerer det ikke.
*/

    // @Override
    // public default boolean equals(Object obj) {
    //     if (obj == this) {
    //         return true;
    //     }
    //     if (!(obj instanceof ITopping)) {
    //         return false;
    //     }
    //     ITopping other = (ITopping) obj;
    //     return this.getName().equals(other.getName()) && this.getCost() == other.getCost();
    // }


}

