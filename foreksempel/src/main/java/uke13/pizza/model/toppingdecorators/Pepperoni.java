package uke13.pizza.model.toppingdecorators;

public class Pepperoni implements ITopping {
    private double cost;

    public Pepperoni(double cost) {
        this.cost = cost;
    }

    public String getName() {
        return "Pepperoni";
    }

    public double getCost() {
        return cost;
    }
}
