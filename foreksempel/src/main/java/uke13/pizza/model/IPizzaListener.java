package uke13.pizza.model;

public interface IPizzaListener {
    void pizzaAdded(PizzaStore store, Pizza pizza);
    void pizzaRemoved(PizzaStore store, Pizza pizza);
}
